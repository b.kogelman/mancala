package nl.sogyo.mancala;

public class Player {
	private String playerName;
	private boolean goAgainBOOL;
	private boolean myTurn;
	public Player opponent; 
	public ParentHole hole0;
	

	public Player(String playerName) {
		this.playerName = playerName;
	}
	
	public void meet(Player opponent) {
		if (this.opponent==null) {
			this.opponent = opponent;
			opponent.meet(this);
		}
		this.myTurn = true;
		if (opponent.isPlaying()) opponent.endTurn();
	}


	public boolean isPlaying() {
		if (this.myTurn == true) return true;
		else return false;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void endTurn() {
		this.myTurn = false;
		if (!opponent.myTurn) opponent.yourTurn();  
	}

	public void yourTurn() {
		this.myTurn = true;
		if (opponent.isPlaying()) opponent.endTurn();
		if (this.canPlay() == false) {
			opponent.endGame();
		}
		


	}

	public void goAgain() {
		this.goAgainBOOL = true;
	}
	
	public boolean canGoAgain() {
		boolean boolToReturn = goAgainBOOL;
		goAgainBOOL = false;
		return boolToReturn;
	}

	public void endGame() {
		System.out.println("No more moves possible");
		//System.exit(0);
	}

	public void setHole0(Hole hole0) {
		this.hole0 = hole0;
	}

	public boolean canPlay() {
		if (hole0.canPlayerPlayAHole(this)) return true;
		else return false;
	}
}
