package nl.sogyo.mancala;

public abstract class ParentHole {


	public int marbleCount = 0;
	public Player owner = null;
	public ParentHole neighbour = null;
	//public ParentHole oppositeNeighbour;


	protected void initializeNeighbour(int totalNumberOfHolesToCreate, int numberOfHolesToInit, Player player0, Player player1, ParentHole hole0) {
		if (numberOfHolesToInit == totalNumberOfHolesToCreate) {
			this.owner = player0;
			hole0 = this;
			neighbour = new Hole(totalNumberOfHolesToCreate, numberOfHolesToInit-1, player0, player1, hole0);
			neighbour.setPlayer(player0);

		}

		else if (numberOfHolesToInit > totalNumberOfHolesToCreate/2+2) {
			neighbour = new Hole(totalNumberOfHolesToCreate, numberOfHolesToInit-1, player0, player1, hole0);
			neighbour.setPlayer(player0);
		}

		else if (numberOfHolesToInit == totalNumberOfHolesToCreate/2+2) {
			neighbour = new Kalaha(totalNumberOfHolesToCreate, numberOfHolesToInit-1, player0, player1, hole0);
			neighbour.setPlayer(player0);
		}

		else if (numberOfHolesToInit > 2) {

			neighbour = new Hole(totalNumberOfHolesToCreate, numberOfHolesToInit-1, player0, player1, hole0);
			neighbour.setPlayer(player1);
		}

		else if (numberOfHolesToInit == 2) {
			neighbour = new Kalaha(totalNumberOfHolesToCreate, numberOfHolesToInit-1, player0, player1, hole0);
			neighbour.setPlayer(player1);
			neighbour.setNeighbour(hole0);
		} 
	}
	
	
	
	protected abstract void receiveMarbles(int NumberOfMarblesReceived);

	public void forwardAndAcceptStolenMarbles(int numberOfStolenMarbles) {
		neighbour.forwardAndAcceptStolenMarbles(numberOfStolenMarbles);
	}
	
	public void checkIfEligibleToSteal() {
	}

	public void stealMarbles() {

	}

	public int turnOverStolenMarbles() {
		return 0;
	}
	
	public int getMarbleCount() {
		return this.marbleCount;
	}


	public int getMarbleCountInKahala(Player player) {
		return -99;
	}
	
	
	public abstract boolean playMarbles();

	public void setMarbleCount(int marbleCount) {
		this.marbleCount = marbleCount;
	}

	public ParentHole getNeighbour(int numberOfNeighboursToFetch) {
		ParentHole parentHole = null;
		if (numberOfNeighboursToFetch > 1 ) {
			parentHole = neighbour.getNeighbour(numberOfNeighboursToFetch-1);
		} else if (numberOfNeighboursToFetch == 1) {
			parentHole = neighbour;	
		}
		else if (numberOfNeighboursToFetch == 0) {
			parentHole = this;
		}
		return parentHole;
	}
	
	
	

	public int getKahalaRelativePosition(Player player, int count) {
		return 0;
	}
	
	
	public ParentHole getOpposite() {
		return null;
	}
	

	public void setPlayer(Player player) {
		this.owner = player;
	}

	public void setNeighbour(ParentHole hole) {
		this.neighbour = hole;
	}

	public int determineNumberOfHolesOfEachPlayer(Player player, int count) {
		return 0;
	}

	public boolean canPlayerPlayAHole(Player player) {
		return false;
	}

	public Kalaha getKahala(Player player) {
		return null;
	}



	public int getMarbleCountFromAllHolesForPlayer(Player player, int i) {
		return 999;
		
	}
	
	public int getMarbleCountFromAllHolesForPlayer(Player player) {
		return 999;
		
	}

}
