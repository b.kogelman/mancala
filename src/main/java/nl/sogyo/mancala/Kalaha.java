package nl.sogyo.mancala;

public class Kalaha extends ParentHole{


	public Kalaha(int totalNumberOfHolesToCreate, int numberOfHolesToInit, Player player0, Player player1, ParentHole hole0) {
		this.marbleCount = 0;
		initializeNeighbour(totalNumberOfHolesToCreate, numberOfHolesToInit,player0, player1, hole0);
	}

	public Kalaha(int numberOfHolesToCreate, Player player0, Player player1) {
		this(numberOfHolesToCreate,numberOfHolesToCreate, player0, player1, null);
	}


	public void receiveMarbles(int NumberOfMarblesReceived) {
		if (owner.isPlaying() && NumberOfMarblesReceived > 1 ) {
			marbleCount+=1;
			neighbour.receiveMarbles(NumberOfMarblesReceived-1);
		}	
		if (owner.isPlaying() && NumberOfMarblesReceived == 1) {
			marbleCount+=1;
			owner.goAgain();
		}
		if (!owner.isPlaying() ) {
			neighbour.receiveMarbles(NumberOfMarblesReceived);

		}
	}

	public void receiveStolenMarbles(int numberOfStolenMarbles) {
		this.marbleCount = this.marbleCount + numberOfStolenMarbles;
	}

	public int getKahalaRelativePosition(Player player, int count) {
		int kahalaPosition = count;
		if (owner == player) {
			kahalaPosition = count;
		}
		if (owner != player) {
			kahalaPosition = neighbour.getKahalaRelativePosition(player, kahalaPosition) + 1;
		}
		return kahalaPosition;
	}

	public Kalaha getKahala(Player player) {
		if (owner == player) return this;
		else return neighbour.getKahala(player);
	}

	public int getMarbleCountInKahala(Player player) {
		if (owner.equals(player)) return this.marbleCount;
		else return getKahala(player.opponent).getMarbleCount();
	}

	public int handOverAllMarbles() {
		System.out.println("I am a kahala. I will not hand over marbles!");
		return 0;
	}

	public void setMarbleCount(int marbleCount) {
		this.marbleCount = marbleCount;
	}

	public int determineNumberOfHolesOfEachPlayer(Player player, int count) {
		int numberOfHoles = count;
		if (neighbour.owner == player) {
			numberOfHoles = neighbour.determineNumberOfHolesOfEachPlayer(player, numberOfHoles);
		}

		return numberOfHoles;
	}

	
	public int getMarbleCountFromAllHolesForPlayer(Player player) {
		return neighbour.getMarbleCountFromAllHolesForPlayer(player, 0);
	}
	
	public int getMarbleCountFromAllHolesForPlayer(Player player, int marbleCount) {
		return marbleCount;
	}

	@Override
	public boolean playMarbles() {
		// TODO Auto-generated method stub
		return false;
	}
}