package nl.sogyo.mancala;

public class Hole extends ParentHole {
	
	public Hole(int totalNumberOfHolesToCreate, int numberOfHolesToInit, Player player0, Player player1, ParentHole hole0) {
		if (totalNumberOfHolesToCreate % 2 != 0) {
			totalNumberOfHolesToCreate--;
		}
		this.marbleCount = 4;
		initializeNeighbour(totalNumberOfHolesToCreate, numberOfHolesToInit,player0, player1, hole0);
	}

	public Hole(int numberOfHolesToCreate, Player player0, Player player1) {
		this(numberOfHolesToCreate,numberOfHolesToCreate, player0, player1, null);
	}

	public Hole(int NumberOfWholesToCreate) {
		this(NumberOfWholesToCreate, new Player("DefaultName0"), new Player("DefaultName1"));
	}

	public boolean playMarbles() {
		if (holeIsPlayeable()) {
			int marblesToPass = this.marbleCount;
			this.marbleCount = 0;
			passMarbles(marblesToPass);		
			return true;
		}
		else {
			System.out.println("Unable to play this hole. Marble count is 0 or opponent tries to play this marble");
			return false;
		}
	}
	
	public boolean holeIsPlayeable() {
		return (this.marbleCount >0 && owner.isPlaying());
	}

	public void passMarbles(int marblesToPass) {
		neighbour.receiveMarbles(marblesToPass);
		if (marblesToPass == 1) {
			neighbour.checkIfEligibleToSteal();
		}
	}
	
	public void receiveMarbles(int numberOfMarblesReceived) {
		marbleCount+=1;
		if (numberOfMarblesReceived > 2) {
			neighbour.receiveMarbles(numberOfMarblesReceived-1);
		}
		if (numberOfMarblesReceived == 2) {
			neighbour.receiveMarbles(1);
			neighbour.checkIfEligibleToSteal();
		}
	}
	
	public void checkIfEligibleToSteal() {
		if (owner.isPlaying() && this.marbleCount == 1) {
			System.out.println(owner.getPlayerName() + " is going to steal marbles from " + owner.opponent.getPlayerName());
			stealMarbles();
		}
	}
	
	public void stealMarbles() {
		int numberOfMarblesToPassIntoKahala = this.marbleCount +  getOpposite().turnOverStolenMarbles();
		this.marbleCount = 0;
		getKahala(this.owner).receiveStolenMarbles(numberOfMarblesToPassIntoKahala);
	}
	
	public int turnOverStolenMarbles() {
		int currentMarbleCount = this.marbleCount;
		this.marbleCount = 0;
		System.out.println("My marbles are stolen :( I had " + currentMarbleCount + " marbles." );
		return currentMarbleCount;
	}
	

	
	public int getMarbleCountInKahala(Player player) {
		return getKahala(player).getMarbleCount();
	}

	public int determineNumberOfHolesOfEachPlayer(Player player, int count) {
		int numberOfHoles = count;
		if (this.owner == player) {
			numberOfHoles = neighbour.determineNumberOfHolesOfEachPlayer(player, numberOfHoles)+1;
		} else {
			numberOfHoles = neighbour.determineNumberOfHolesOfEachPlayer(player, numberOfHoles);
		}
		return numberOfHoles;
	}

	public int getStartPositionOfPlayer(Player player) {
		int startPosition = -1;
		if (this.owner == player) startPosition = 0;
		if (this.owner != player) startPosition = getKahalaRelativePosition(player.opponent) + 1;
		return startPosition;
	}

	public ParentHole getOpposite() {
		int nextKahalaPosition = getKahalaRelativePosition(this.owner); 
		ParentHole opposite = getNeighbour(nextKahalaPosition*2);
		return opposite;
	}
	
	public int getKahalaRelativePosition(Player player, int count) {
		int kahalaPosition = count;
		kahalaPosition = neighbour.getKahalaRelativePosition(player, kahalaPosition) + 1;
		return kahalaPosition;
	}
	
	public int getKahalaRelativePosition(Player player) {
		return getKahalaRelativePosition(player, 0);
	}
	
	public Kalaha getKahala(Player player) {
		return neighbour.getKahala(player);
	}
	
	public boolean canPlayerPlayAHole(Player player) {
		if (getMarbleCountFromAllHolesForPlayer(player) == 0) return false;
		else return true;
	}
	
	public int getMarbleCountFromAllHolesForPlayer(Player player) {
		return getKahala(player.opponent).getMarbleCountFromAllHolesForPlayer(player);
	}
	
	public int getMarbleCountFromAllHolesForPlayer(Player player, int marbleCount) {
		return neighbour.getMarbleCountFromAllHolesForPlayer(player, this.marbleCount + marbleCount );
	}
	
	public void meetPlayers() {
		owner.setHole0(this);
		owner.opponent.setHole0(this);
	}

}
