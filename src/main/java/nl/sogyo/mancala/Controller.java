package nl.sogyo.mancala;

import java.util.Scanner;

public class Controller {

	Player player0;
	Player player1;
	Player currentPlayer;
	Hole hole0;
	private int numberOfHolesToCreate = 14;
	boolean gameOver = false;

	public void initializeGame() {
		player0 = new Player("Player 1");
		player1 = new Player("Player 2");
		player0.meet(player1);

		hole0 = new Hole(numberOfHolesToCreate, player0, player1);
		hole0.meetPlayers();
		player0.yourTurn();
	}
	
	public void manipulateInitialGamePlayer0() {
		for (int hole=0; hole <5; hole++ ) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
	}
	public void manipulateInitialGamePlayer1() {
		for (int hole=7; hole <12; hole++ ) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
	}

	public void play() {
		Scanner sc = new Scanner(System.in);
		showBoard();
		
		while(!gameOver) {
		
			if (player0.isPlaying()) currentPlayer = player0;
			else currentPlayer = player1;
				
			if (!currentPlayer.canPlay()) {
				gameOver();
			}		
			showBoard();

			boolean invalidChoice = false;
			do {
				System.out.println(currentPlayer.getPlayerName() + " it is your turn");
				System.out.println("Please select which hole you like to play");
				int i = sc.nextInt();
				if (i < 1 || i > 6) {
					invalidChoice = true;
					System.out.println("Choice a hole between 1 and 6.");
				}
				 else {
					if( hole0.getKahala(currentPlayer.opponent).getNeighbour(i).playMarbles()) invalidChoice = false;
				 else invalidChoice = true;} 
				
				
			} while (invalidChoice);

			
			
			
			

			if (!currentPlayer.canGoAgain()) {
				currentPlayer.endTurn();
			}
			
			
	





		}
		sc.close();
	}
	

	private void gameOver() {
		System.out.println("Game over" );
		int scorePlayer0 = hole0.getMarbleCountInKahala(player0);
		int scorePlayer1 =  hole0.getMarbleCountInKahala(player1);
		System.out.println("The score is ---> player 1: " + scorePlayer0 + " and player 2 : " + scorePlayer1);
		
		if (scorePlayer0> scorePlayer1) {
			System.out.println(player0.getPlayerName()  + " has won!");
		}
		
		if (scorePlayer0 < scorePlayer1) {
			System.out.println(player1.getPlayerName() + " has won!");
		}
		
		if (scorePlayer0 == scorePlayer1) {
			System.out.println("Equal score!!!");
		}
		System.exit(0);
		
	}

	public void showBoard() {
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("                     Kalaha of player " + hole0.getKahala(player1).owner.getPlayerName() + " contains " + hole0.getKahala(player1).getMarbleCount() + " marbles");
		System.out.println("---------------------------------------------------------------------------------------------------");
	
		for (int hole = 1; hole < 7; hole++ ) {
			System.out.println("---------------------------------------------------------------------------------------------------");
			System.out.print("Hole " + (hole) + " owned by " + hole0.getKahala(player1).getNeighbour(hole).owner.getPlayerName() + " contains " + hole0.getKahala(player1).getNeighbour(hole).getMarbleCount()+ " marbles" + "             ");
			System.out.print("Hole " + (7-hole) + " owned by " + hole0.getKahala(player0).getNeighbour(7-hole).owner.getPlayerName() + " contains " + hole0.getKahala(player0).getNeighbour(7-hole).getMarbleCount()+ " marbles" + "\n");
		}
		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("                     Kalaha of player " + hole0.getKahala(player0).owner.getPlayerName() + " contains " + hole0.getKahala(player0).getMarbleCount() + " marbles");
		System.out.println("---------------------------------------------------------------------------------------------------");
		
		System.out.println("\n\n");
	}
        
        public int getNumberOfHolesToCreate() {
            return this.numberOfHolesToCreate;
        }



}
