package nl.sogyo.mancala;
import org.junit.Assert;
import org.junit.Test;

public class HoleTest {
	Player player0;// = new Player("Speler 0");
	Player player1;// = new Player("Speler 1");
	Hole hole0;// = new Hole(14, player0, player1);

	public void initializeGame() {
		player0 = new Player("Speler 0");
		player1 = new Player("Speler 1");
		player0.meet(player1);
		hole0 = new Hole(14, player0, player1);
		hole0.meetPlayers();
	}

	public void testDefaultMarbleCount() {
		Hole hole0 = new Hole(6); 
		Assert.assertTrue(hole0.getMarbleCount()==4);	
	}

	@Test
	public void testMarbleCountAfterPlayMarble() {
		initializeGame();
		hole0.playMarbles();
		Assert.assertTrue(hole0.getMarbleCount()==0);
	}

	@Test
	public void testReceiveMarbles() {
		initializeGame();
		hole0.receiveMarbles(3);
		Assert.assertTrue(hole0.getMarbleCount()==5);
	}






	@Test
	public void testNeigbhourAfterPlayMarbles() {
		initializeGame();
		hole0.playMarbles();
		Assert.assertTrue((hole0.neighbour.getMarbleCount() == 5));
		//Assert.assertTrue(hole0.nextNeighbour.nextNeighbour.getMarbleCount() == 5);
	}

	@Test
	public void testSecondNeigherAfterPlayMarbles() {
		initializeGame();
		hole0.playMarbles();
		Assert.assertTrue(hole0.neighbour.neighbour.getMarbleCount() == 5);
	}


	// TEST DETERMINE NUMBER OF HOLES 
	@Test
	public void testdetermineNumberOfHoles() {
		initializeGame();
		Assert.assertTrue(hole0.determineNumberOfHolesOfEachPlayer(player0, 0) == 6);
		Assert.assertTrue(hole0.determineNumberOfHolesOfEachPlayer(player1, 0) == 6);
	}








	// TESTING GET_MARBLE_COUNT_IN_ALL_NEIGHBOURS FUNCTION
	@Test
	public void testMarbleCountInAllNeighbours() {
		initializeGame();
		System.out.println("\n\n\n\n ");
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4);
	}



	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesInFirstHole() {
		initializeGame();
		hole0.playMarbles();
		
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4);
	}

	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesInSecondHole() {
		initializeGame();
		hole0.neighbour.playMarbles();
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4);
	}

	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesInFourthHole() {
		initializeGame();
		hole0.getNeighbour(3).playMarbles();
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4-2);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4+1);
	}

	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesInSixthHole() {
		initializeGame();
		hole0.getNeighbour(5).playMarbles();
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4-4);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4+3);
	}
	
	
	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesOpponentFirstHole() {
		initializeGame();
		hole0.getNeighbour(hole0.getStartPositionOfPlayer(player1)).playMarbles();
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4);
	}
	
	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesOpponentFifthHole() {
		initializeGame();
		player0.endTurn();
		Assert.assertTrue(player1.isPlaying());
		Assert.assertFalse(player0.isPlaying());
		hole0.getNeighbour(hole0.getStartPositionOfPlayer(player1) + 4).playMarbles();
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4+2);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4-3);
	}
	
	@Test
	public void testMarbleCountInAllNeighboursAfterPlayMarblesOpponentFifthHoleOpponentPlayer() {
		initializeGame();
		player1.yourTurn();
		hole0.getNeighbour(11).playMarbles();
		Assert.assertTrue(player1.isPlaying());
		Assert.assertFalse(player0.isPlaying());
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 6*4+2);
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 6*4-3);
		Assert.assertTrue(hole0.getMarbleCountInKahala(player0) == 0);
		Assert.assertTrue(hole0.getMarbleCountInKahala(player1) == 1);
	}
	
	@Test
	public void testMarbleCountInEmptyRow() {
		initializeGame();
		for (int hole = 0; hole<6; hole++) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player0) == 0);
	}
		
	@Test
	public void testMarbleCountInEmptyRowOpponent() {
		initializeGame();
		for (int hole = 6; hole<14; hole++) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		Assert.assertTrue(hole0.getMarbleCountFromAllHolesForPlayer(player1) == 0);
	}










	
	// PLAYERS PLAYEBLE??
	@Test
	public void testPlayerPlayeble() {
		initializeGame();
		for (int hole = 0; hole<6; hole++) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		
		Assert.assertFalse(hole0.canPlayerPlayAHole(player0));
		Assert.assertTrue(hole0.canPlayerPlayAHole(player1));
	}
		
	@Test
	public void testPlayerOpponentPlayeble() {
		initializeGame();
		for (int hole = 6; hole<14; hole++) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		
		Assert.assertTrue(hole0.canPlayerPlayAHole(player0));
		Assert.assertFalse(hole0.canPlayerPlayAHole(player1));
	}
	
	
	@Test
	public void testHole0MeetPlayers() {
		initializeGame();
		
		hole0.setMarbleCount(16);
		
		Assert.assertTrue(player0.hole0.getMarbleCount() == 16);
		Assert.assertTrue(player1.hole0.getMarbleCount() == 16);
	
	}

	
	@Test
	public void testStealMarbles() {
		
		initializeGame();
		Assert.assertTrue(player0.isPlaying());
		for (int hole = 0; hole < 5; hole ++ ) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		hole0.getNeighbour(5).setMarbleCount(10);
		hole0.getNeighbour(5).playMarbles();
		Assert.assertTrue(hole0.getMarbleCountInKahala(player0) == 7);
	}
	
	
	
	// GET KAHALA POSITION
	
	@Test
	public void testGetKahalaPosition() {
		initializeGame();
		Assert.assertTrue(hole0.getKahalaRelativePosition(player0, 0) == 6);
		Assert.assertTrue(hole0.getKahalaRelativePosition(player1, 0) == 13);
	}

}
