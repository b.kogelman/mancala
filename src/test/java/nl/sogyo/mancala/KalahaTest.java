package nl.sogyo.mancala;
import org.junit.Assert;
import org.junit.Test;

public class KalahaTest  {

	
	Player player0;// = new Player("Speler 0");
	Player player1;// = new Player("Speler 1");
	Hole hole0;// = new Hole(14, player0, player1);

	public void initializeGame() {
		player0 = new Player("Speler 0");
		player1 = new Player("Speler 1");
		player0.meet(player1);
		hole0 = new Hole(14, player0, player1);
		hole0.meetPlayers();
	}
	
	
	@Test
	public void testMarbleCount() {
		initializeGame();
		Kalaha kalaha = new Kalaha(14,player0, player1);
		Assert.assertTrue(kalaha.getMarbleCount()==0);
	}
	
	@Test
	public void getKahalaPlayer1() {
		initializeGame();
		Kalaha test = hole0.getKahala(player0);
		Assert.assertTrue(test.owner.getPlayerName() == player0.getPlayerName());
	}
	
	@Test
	public void getKahalaPlayer2() {
		initializeGame();
		Kalaha test = hole0.getKahala(player1);
		Assert.assertTrue(test.owner.getPlayerName() == player1.getPlayerName());
	}

}
