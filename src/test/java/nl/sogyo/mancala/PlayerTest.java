package nl.sogyo.mancala;
import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {

	private String playerZeroName = "Speler 0";
	private String playerOneName = "Speler 1";	
	Player player0;
	Player player1;
	Hole hole0;

	public void initializeGame() {
		player0 = new Player("Speler 0");
		player1 = new Player("Speler 1");
		player0.meet(player1);
		hole0 = new Hole(14, player0, player1);
		hole0.meetPlayers();
	}
	
	
	
	
	
	@Test 
	public void testPlayerNameAllocation() {
		Player player0 = new Player(playerZeroName);
		Player player1 = new Player(playerOneName);
		player0.meet(player1);
		
		Assert.assertTrue(player0.getPlayerName().equals(playerZeroName));
		Assert.assertTrue(player1.getPlayerName().equals(playerOneName));
	}
	
	@Test
	public void testMeetAndGreet() {
		Player player0 = new Player(playerZeroName);
		Player player1 = new Player(playerOneName);
		player0.meet(player1);
		
		Assert.assertTrue(player0.opponent.getPlayerName().equals(playerOneName));
		Assert.assertTrue(player1.opponent.getPlayerName().equals(playerZeroName));
	}
	
	@Test
	public void testInitialTurnsMyTurn() {
		initializeGame();
		
		Assert.assertTrue(player0.isPlaying());
		Assert.assertFalse(player1.isPlaying());
	}
	
	@Test
	public void testEndTurn() {
		initializeGame();
		
		player0.endTurn();
		Assert.assertFalse(player0.isPlaying());
		Assert.assertTrue(player1.isPlaying());
	}
	
	@Test 
	public void testyourTurn() {
		initializeGame();
		
		player0.endTurn();
		player0.yourTurn();
		
		Assert.assertTrue(player0.isPlaying());
		Assert.assertFalse(player1.isPlaying());
	}
	
	@Test
	public void testGoAgain() {
		initializeGame();
		
		player0.endTurn();
		player0.yourTurn();
		player0.goAgain();
		Assert.assertTrue(player0.isPlaying());
		Assert.assertFalse(player1.isPlaying());
	}
	
	
	@Test 
	public void testIfPlayerCanPlay() {
		initializeGame();
		// Set all holes to zero
		for (int hole = 0; hole < 6; hole ++ ) {
			hole0.getNeighbour(hole).setMarbleCount(0);
		}
		
		Assert.assertFalse(player0.canPlay() == true);
		Assert.assertTrue(player1.canPlay() == true);
		}
	
	
}
